package org.example.framework.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
    private String method;
    private String path;
    private Matcher pathMatcher;
    private String query;
    private Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private String httpVersion;
    private Map<String, String> headers = new LinkedHashMap<>();
    private byte[] body;
    private Principal principal;
    private Map<String, List<String>> formParams = new LinkedHashMap<>();

    public void setHeader(String key, String value) {
        headers.put(key, value);
    }

    public Optional<String> getQueryParam(final String name) {
        if (!queryParams.containsKey(name)) {
            return Optional.empty();
        } else {
            final List<String> values = queryParams.get(name);
            return Optional.of(values.get(0));
        }
    }

    public List<String> getQueryParams(final String name) {
        return queryParams.getOrDefault(name, null);
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }

    public Optional<String> getFormParam(final String name) {
        if (!formParams.containsKey(name)) {
            return Optional.empty();
        } else {
            final List<String> values = formParams.get(name);
            return Optional.of(values.get(0));
        }
    }

    public List<String> getFormParams(final String name) {
        return formParams.getOrDefault(name, null);
    }

    public Map<String, List<String>> getAllFormParams() {
        return formParams;
    }
}
